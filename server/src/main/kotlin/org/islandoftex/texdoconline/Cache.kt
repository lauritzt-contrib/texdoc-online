// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline

import kotlinx.datetime.*
import org.islandoftex.ctanapi.CTANTopic

/**
 * A cache for CTAN topics.
 */
class Cache() {
    /**
     * An internal constructor to allow for different expiration times.
     */
    internal constructor(expireAfter: DateTimePeriod) : this() {
        expirationInterval = expireAfter
    }

    // define the expiration interval, i.e. the time frame which will be the TTL for a refreshed cache,
    // the expiration timestamp as the concrete instantiation of the time of refreshing plus the expiration
    // interval and the expired value to check whether the current cache is expired
    private var expirationInterval = DateTimePeriod(hours = 12)
    private var expirationTimestamp = Clock.System.now().plus(expirationInterval, TimeZone.currentSystemDefault())
    private val expired: Boolean
        get() = expirationTimestamp.toEpochMilliseconds() < Clock.System.now().toEpochMilliseconds()

    private val topicCache = mutableMapOf<String, CTANTopic>()

    private fun updateMap(content: Map<String, CTANTopic>) {
        topicCache.clear()
        topicCache.putAll(content)
        expirationTimestamp = Clock.System.now().plus(expirationInterval, TimeZone.currentSystemDefault())
    }

    /**
     * Get a [CTANTopic] from the cache.
     *
     * @param key The name of the CTAN topic.
     * @param fetch A lambda to fetch all topics.
     */
    fun getOrElse(key: String, fetch: () -> Map<String, CTANTopic>): CTANTopic {
        if (topicCache.isEmpty() || expired) {
            updateMap(fetch())
        }
        if (!topicCache.containsKey(key)) {
            throw NoSuchElementException("Key does not exist in cache")
        } else {
            return topicCache[key]!!
        }
    }

    /**
     * Get a map of [CTANTopic] entries from the cache.
     *
     * @param fetch A lambda to fetch all topics.
     */

    fun getAllOrElse(fetch: () -> Map<String, CTANTopic>): Map<String, CTANTopic> {
        if (topicCache.isEmpty() || expired) {
            updateMap(fetch())
        }
        return topicCache
    }

}
