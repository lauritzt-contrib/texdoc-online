// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline

import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.shouldBe
import kotlinx.datetime.DateTimePeriod
import org.islandoftex.ctanapi.CTANTopic

class CacheTest: ShouldSpec({
    val cache = Cache(DateTimePeriod(seconds = 10))

    context("Key insertion") {
        val entries = mapOf(
                "foo" to CTANTopic("foo", "foo"),
                "bar" to CTANTopic("bar", "bar")
        )

        should("add entries to cache successfully") {
            val result = cache.getAllOrElse { entries }
            result.size shouldBe 2
            result["foo"] shouldBe entries["foo"]
            result["bar"] shouldBe entries["bar"]
        }
    }

    context("Key retrieval") {
        val bad = mapOf(
                "foo" to CTANTopic("fooz", "fooz"),
                "bar" to CTANTopic("baz", "baz")
        )

        should("get key 1 successfully") {
            val key1 = cache.getOrElse("foo") { bad }
            key1.name shouldBe "foo"
            key1.details shouldBe "foo"
        }

        should("get key 2 successfully") {
            val key2 = cache.getOrElse("bar") { bad }
            key2.name shouldBe "bar"
            key2.details shouldBe "bar"
        }

        should("throw an exception for an nonexisting key") {
            shouldThrow<NoSuchElementException> {
                cache.getOrElse("top") {
                    mapOf()
                }
            }
        }
    }
})
