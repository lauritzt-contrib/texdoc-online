# pull the latest TeX Live with documentation files available (no source files!)
FROM registry.gitlab.com/islandoftex/images/texlive:latest-doc

# install curl to get supercronic and gpg to verify TeX Live updates
RUN apt-get update && apt-get install -y curl gpg sudo && rm -rf /var/lib/apt/lists/*

# install supercronic from their releases page (https://github.com/aptible/supercronic/releases)
ENV SUPERCRONIC_URL=https://github.com/aptible/supercronic/releases/download/v0.1.11/supercronic-linux-amd64 \
    SUPERCRONIC=supercronic-linux-amd64 \
    SUPERCRONIC_SHA1SUM=a2e2d47078a8dafc5949491e5ea7267cc721d67c

RUN curl -fsSLO "$SUPERCRONIC_URL" \
 && echo "${SUPERCRONIC_SHA1SUM}  ${SUPERCRONIC}" | sha1sum -c - \
 && chmod +x "$SUPERCRONIC" \
 && mv "$SUPERCRONIC" "/usr/local/bin/${SUPERCRONIC}" \
 && ln -s "/usr/local/bin/${SUPERCRONIC}" /usr/local/bin/supercronic

# create the software's directory
WORKDIR /texdoconline

# add an own user for the application and give sudo rights on tlmgr
RUN groupadd -r texdoconline && useradd --no-log-init -r -g texdoconline texdoconline
RUN echo "texdoconline ALL = (root) NOPASSWD: `which tlmgr`" >> /etc/sudoers

# add local scripts and the software bundle to the Docker image
ADD docker/tlupdate.sh ./tlupdate.sh
ADD docker/entrypoint.sh ./entrypoint.sh
RUN chmod +x ./*.sh && mkdir -p includes/js && mkdir includes/css && mkdir includes/fonts
ADD bin/*-with-deps-*.jar ./
ADD https://code.iconify.design/1/1.0.7/iconify.min.js includes/js/iconify.min.js
ADD docker/fonts.css includes/css/fonts.css
RUN curl -fsSL -o nunito.zip https://fonts.google.com/download?family=Nunito%20Sans && \
  unzip nunito.zip -d includes/fonts && rm nunito.zip && \
  curl -fsSL -o montserrat.zip https://fonts.google.com/download?family=Montserrat && \
  unzip -o montserrat.zip -d includes/fonts && rm montserrat.zip

# switch user context
RUN chown -R texdoconline ./
USER texdoconline

# texdoc online runs and listens on port 8080 and will be started by the entrypoint script
EXPOSE 8080
ENTRYPOINT ["./entrypoint.sh", "-s", "/texdoconline/includes"]
