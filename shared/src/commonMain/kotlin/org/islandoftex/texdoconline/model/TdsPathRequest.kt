// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.model

import kotlinx.serialization.Serializable

/**
 * A request to access a specific file from the TDS.
 */
@Serializable
public data class TdsPathRequest(
    /**
     * The path within the tds, e.g. doc/generic/pgf/pgfmanual.pdf
     */
    val path: String
)
