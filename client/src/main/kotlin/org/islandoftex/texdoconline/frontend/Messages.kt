// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.frontend

import kotlinx.browser.document
import kotlinx.html.dom.create
import kotlinx.html.js.div
import kotlinx.html.p
import kotlinx.html.span

internal object Messages {
    val topicsNoEntries = document.create.p("u-text-center") {
        span("iconify") {
            attributes["data-icon"] = "la:heart-broken-solid"
            attributes["data-icon-inline"] = "true"
        }
        +" No topics found. Please wait a few moments and try again."
    }

    val topicTagsNoEntries = document.create.div("tag-separate") {
        span("iconify") {
            attributes["data-icon"] = "la:heart-broken-solid"
            attributes["data-icon-inline"] = "true"
        }
        +" No packages found." // TODO: shouldn't this be in a p?
    }

    val topicSearchNoPackagesFound = document.create.p("u-text-center") {
        span("iconify") {
            attributes["data-icon"] = "la:heart-broken-solid"
            attributes["data-icon-inline"] = "true"
        }
        +" No packages found. Please try again with a different topic."
    }

    val packageSearchNoHits = document.create.p("u-text-center") {
        span("iconify") {
            attributes["data-icon"] = "la:heart-broken-solid"
            attributes["data-icon-inline"] = "true"
        }
        +" No hits found. Please try again with a different keyword."
    }

    val packageSearchEmptyQuery = document.create.p("u-text-center") {
        span("iconify") {
            attributes["data-icon"] = "la:search-solid"
            attributes["data-icon-inline"] = "true"
        }
        +" Please type a valid package name and try again."
    }

    val notifyAPIDown = document.create.p("u-text-center") {
        span("iconify") {
            attributes["data-icon"] = "la:satellite-dish-solid"
            attributes["data-icon-inline"] = "true"
        }
        +" The API endpoint seems to be unreachable. Please wait a few moments and try again."
    }

    val notifyAPIDownShort = document.create.div("tag-separate") {
        span("iconify") {
            attributes["data-icon"] = "la:satellite-dish-solid"
            attributes["data-icon-inline"] = "true"
        }
        +" The API endpoint seems to be unreachable."
    }
}
