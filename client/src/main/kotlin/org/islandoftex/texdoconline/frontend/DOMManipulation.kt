// SPDX-License-Identifier: BSD-3-Clause
package org.islandoftex.texdoconline.frontend

import io.ktor.client.*
import io.ktor.client.engine.js.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.request.*
import io.ktor.utils.io.core.*
import kotlinx.browser.document
import kotlinx.browser.window
import kotlinx.dom.clear
import kotlinx.html.*
import kotlinx.html.dom.append
import org.islandoftex.texdoconline.model.InternalCTANTopic
import org.islandoftex.texdoconline.model.InternalTeXdocEntry
import org.islandoftex.texdoconline.model.VersionInformation
import org.w3c.dom.HTMLDivElement
import org.w3c.dom.HTMLInputElement
import org.w3c.dom.Node
import kotlin.js.Date
import kotlin.math.min

internal object DOMManipulation {
    /**
     * Perform an HTTP GET request to the given [url] returning a
     * deserialized object of type [T].
     *
     * @param T The type of the JSON object.
     * @param url The URL of the resource outputting JSON.
     */
    private suspend inline fun <reified T> httpRequest(url: String): T =
        HttpClient(Js) {
            install(JsonFeature) {
                serializer = KotlinxSerializer()
            }
        }.use {
            it.get(url)
        }

    /**
     * Replace the innerHTML content of a node by another (single) node.
     *
     * @param node The new single child.
     */
    private fun Node?.replaceContent(node: Node) {
        this?.clear()
        this?.appendChild(node)
    }

    /**
     * The current path to our resource including the protocol and the host including the port.
     */
    private val currentPath: String
        get() = window.location.protocol + "//" + window.location.host

    /**
     * Update the search query input field and perform the search.
     *
     * @param query The term to query.
     */
    suspend fun searchThis(query: String) {
        (document.getElementById("searchquery") as HTMLInputElement?)?.value = query
        performSearchQuery()
    }

    /**
     * Perform the search query. This retrieves the query string from the input field and uses
     * that to search for matching documentation files.
     */
    suspend fun performSearchQuery() {
        val pkg = (document.getElementById("searchquery") as HTMLInputElement?)?.value?.trim()
        if (pkg.isNullOrBlank()) {
            document.getElementById("searchresults")?.replaceContent(Messages.packageSearchEmptyQuery)
            return
        }

        (document.getElementById("searchquery") as HTMLInputElement?)?.value = pkg
        document.getElementById("searchresults")?.replaceContent(RendererUtils.drawSpinner("Fetching results..."))

        try {
            httpRequest<List<InternalTeXdocEntry>>("$currentPath/texdoc/$pkg").run {
                (document.getElementById("searchresults") as HTMLDivElement?).replaceContent(
                    if (isEmpty()) {
                        Messages.packageSearchNoHits
                    } else {
                        RendererUtils.packageSearchBuildTable(this.mapIndexed { index, entry ->
                            RendererUtils.packageSearchTableRow(entry.path, entry.description, pkg, index)
                        })
                    }
                )
            }
        } catch (_: Exception) {
            // TODO: refine
            document.getElementById("searchresults")?.replaceContent(Messages.notifyAPIDown)
        }
    }

    /**
     * Update the version information shown in the page's footer.
     */
    suspend fun fillVersionContainer() {
        document.getElementById("version-container")?.replaceContent(RendererUtils.drawSpinnerSmall())
        httpRequest<VersionInformation>("$currentPath/version").run {
            val date = Date(Date.parse(tlpdb))
            val dateOptions = dateLocaleOptions {
                weekday = "short"
                year = "numeric"
                month = "long"
                day = "numeric"
            }
            val versionContainer = document.getElementById("version-container") as HTMLDivElement?
            versionContainer?.clear()
            versionContainer?.append {
                p("small-text u-text-center") {
                    tex()
                    +"doc online API: "
                    +api
                    span("iconify") {
                        attributes["data-icon"] = "la:ellipsis-v-solid"
                        attributes["data-icon-inline"] = "true"
                    }
                    tex()
                    +"doc version: "
                    +texdoc
                    span("iconify") {
                        attributes["data-icon"] = "la:ellipsis-v-solid"
                        attributes["data-icon-inline"] = "true"
                    }
                    +"Last "
                    tex()
                    +" Live update: "
                    +date.toLocaleDateString(arrayOf(), dateOptions)
                }
            }
        }
    }

    /**
     * Fetch a list of 6 topics to be shown as promoted topics on the front page.
     */
    suspend fun fetchTopics() {
        val topicContainer = document.getElementById("topics") ?: return

        topicContainer.replaceContent(RendererUtils.drawSpinner("Loading topics..."))
        try {
            document.getElementById("topic-header")?.innerHTML = "Topics"
            val highlights = mutableListOf<InternalCTANTopic>()
            httpRequest<List<InternalCTANTopic>>("$currentPath/topics/list").run {
                topicContainer.replaceContent(
                    if (isEmpty()) {
                        Messages.topicsNoEntries
                    } else {
                        val size = min(6, size)
                        highlights.addAll(this.shuffled().subList(0, size))
                        RendererUtils.buildTopicCards(highlights.map {
                            RendererUtils.buildTopicCard(it.key, it.details)
                        })
                    }
                )
            }
            highlights.forEach {
                fetchTopicTags(it.key)
            }
        } catch (_: Exception) {
            // TODO: refine
            document.getElementById("topics")?.replaceContent(Messages.notifyAPIDown)
        }
    }

    /**
     * Fetch 4 packages for exemplary display in a topic's card.
     *
     * @param topic The topic.
     */
    private suspend fun fetchTopicTags(topic: String) {
        document.getElementById("id-$topic-pkglist")?.replaceContent(RendererUtils.drawSpinnerSmall())
        try {
            httpRequest<InternalCTANTopic>("$currentPath/topic/$topic").run {
                (document.getElementById("id-$topic-pkglist") as HTMLDivElement?).replaceContent(
                    if (packages.isEmpty()) {
                        Messages.topicTagsNoEntries
                    } else {
                        val size = min(4, packages.size)
                        val highlights = packages.shuffled().subList(0, size)
                        RendererUtils.buildTopicPackageTags(highlights.map {
                            RendererUtils.buildTopicPackageTag(it)
                        })
                    }
                )
            }
        } catch (_: Exception) {
            // TODO: refine
            document.getElementById("id-$topic-pkglist")?.replaceContent(Messages.notifyAPIDownShort)
        }
    }

    /**
     * Generate the table of packages found for [topic].
     *
     * @param topic The topic to search for packages.
     */
    suspend fun fetchTopicTable(topic: String) {
        document.getElementById("topics")?.replaceContent(RendererUtils.drawSpinner("Loading packages..."))
        try {
            document.getElementById("topic-header")?.innerHTML = "Packages listed in <i>$topic</i>"
            httpRequest<InternalCTANTopic>("$currentPath/topic/$topic").run {
                (document.getElementById("topics") as HTMLDivElement?).replaceContent(
                    if (packages.isEmpty()) {
                        Messages.topicSearchNoPackagesFound
                    } else {
                        RendererUtils.drawPackageTable(packages.map {
                            RendererUtils.drawPackageTableRow(it)
                        })
                    }
                )
            }
        } catch (_: Exception) {
            // TODO: refine
            document.getElementById("topics")?.replaceContent(Messages.notifyAPIDown)
        }
    }

    /**
     * Fetch all topics CTAN knows to be shown on the front page. If showing all topics,
     * no package tags will be generated.
     */
    suspend fun fetchAllTopics() {
        document.getElementById("topics")?.replaceContent(RendererUtils.drawSpinner("Loading all topics..."))
        try {
            document.getElementById("topic-header")?.innerHTML = "Topics"
            httpRequest<List<InternalCTANTopic>>("$currentPath/topics/list").run {
                (document.getElementById("topics") as HTMLDivElement?).replaceContent(
                    if (isEmpty()) {
                        Messages.topicsNoEntries
                    } else {
                        RendererUtils.buildTopicCards(this.map {
                            RendererUtils.buildTopicCard(it.key, it.details)
                        })
                    }
                )
            }
        } catch (_: Exception) {
            // TODO: refine
            document.getElementById("topics")?.replaceContent(Messages.notifyAPIDown)
        }
    }
}
